# fbx-tree-view

`fbx-tree-view` is a viewer of internal FBX node tree (and their properties).
This may be useful for developers who implement FBX loader.

## Dependencies

This software uses GTK+ 3.

## Screenshot

![screenshot1](./resources/screenshot1.png)

## License

Licensed under either of

* Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE.txt) or https://www.apache.org/licenses/LICENSE-2.0 )
* MIT license ([LICENSE-MIT](LICENSE-MIT.txt) or https://opensource.org/licenses/MIT )

at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted for inclusion in the work by you,
as defined in the Apache-2.0 license, shall be dual licensed as above, without any additional terms or conditions.
